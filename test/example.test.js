const { expect } = require('chai')
const mylib = require('../src/mylib')

describe("My unit tests", () => {
	before(() => {
		// do something before testing.
		console.log("test database initialized")
	})
	it(`Adds 2 + 2 and equals 4`, () => {
		expect(mylib.add(2, 2)).to.equal(4)
	})
	it("Subtracts 5 - 2 and equals 3", () => {
		expect(mylib.subtract(5, 2)).to.equal(3)
	})
	it("Fails if divisor is 0", () => {
		// expect something to throw an error.
		expect(() => mylib.divide(9, 0)).to.throw()
	})
	it("Multiplys 6 * 3 and equals 18", () => {
		expect(mylib.multiply(6, 3)).to.equal(18)
	})
	after(() => {
		// do something after the testing.
		console.log("test database connection closed")
	})
})