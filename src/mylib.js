/** Basic arithmetic operations */
const mylib = {
	/** Multiline arrow function. 
	* @param {Number} a is the first number
	* @param {Number} b is the second number
	* @return {Number} This is the sum of the a and b
	*/
	add: (a, b) => {
		const sum = a + b
		return sum
	},
	/** Multiline arrow function. 
	* @param {Number} a is the first number
	* @param {Number} b is the second number
	* @return {Number} This is the difference of the a and b
	*/
	subtract: (a, b) => {
		return a - b
	},
	/** Multiline arrow function. 
	* @param {Number} dividend is the number to be divided
	* @param {Number} divisor is the number to divide dividend
	* @return {Number} This is the quotient of the division
	*/
	divide: (dividend, divisor) => {
		if (divisor === 0) {
			throw new Error('divisor 0 not allowed')
		}
		return dividend / divisor
	},
	/** Regular function 
	* @param {Number} a is the first number
	* @param {Number} b is the second number
	* @return {Number} This is the result of multiplication
	*/
	multiply: function(a, b) {
		return a * b
	}
}

module.exports = mylib